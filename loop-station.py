#!/usr/bin/env python

import glob
import os
import re
import signal
import threading
import time
from sys import exit
from random import randint

try:
    import pygame
except ImportError:
    exit("This script requires the pygame module\nInstall with: sudo pip install pygame")

import pianohat

BANK = os.path.join(os.path.dirname(__file__), "sounds")

FILETYPES = ['*.wav', '*.ogg']
samples = []
files = []
octave = 0
octaves = 0

pygame.mixer.pre_init(44100, -16, 1, 512)
pygame.mixer.init()
pygame.mixer.set_num_channels(32)

patches = glob.glob(os.path.join(BANK, '*'))
patch_index = 1

loop_length = -1
start_time = -1
first_loop_notes = []
loops = [[]]
layering_thread = None
instrument_pressed = False
in_loop_mode = True

if len(patches) == 0:
    exit("Couldn't find any .wav files in: {}".format(BANK))

def reset():
    global loop_length, start_time, first_loop_notes, loops, layering_thread, instrument_pressed, in_loop_mode
    loop_length = -1
    start_time = -1
    first_loop_notes = []
    loops = [[]]
    layering_thread = None
    instrument_pressed = False
    in_loop_mode = True

def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower() for text in re.split(_nsre, s)]

def load_samples(patch):
    global samples, files, octaves, octave
    files = []
    print('Loading Samples from: {}'.format(patch))
    for filetype in FILETYPES:
        files.extend(glob.glob(os.path.join(patch, filetype)))
    files.sort(key=natural_sort_key)
    octaves = len(files) / 12
    samples = [pygame.mixer.Sound(sample) for sample in files]
    octave = int(octaves / 2)

def handle_note(channel, pressed):
    global start_time, loop_length, first_loop_notes, instrument_pressed, loops, in_loop_mode
    if instrument_pressed:
        instrument_pressed = False
        if channel == 12:
            print('Stopping and Deleting Loop.')
            for loop in loops:
                for thread in loop:
                    thread.cancel()
                reset()
        elif channel == 0:
            print('Setting Loop Mode to {}'.format(not in_loop_mode))
            in_loop_mode = not in_loop_mode
        elif channel == 5:
            print('Deleting Last Loop Layer.')
            delete_index = len(loops)-2
            if len(loops[len(loops)-1]) > 0:
                delete_index = len(loops)-1
            for thread in loops[delete_index]:
                thread.cancel()
            del loops[delete_index]
            if len(loops[len(loops)-1]) > 0:
                loops.append([])
            if len(loops) == 1:
                reset()
    else:
        channel = channel + (12 * octave) + 3
        if channel < len(samples) and pressed:
            samples[channel].play(loops=0)
            if start_time == -1:
                start_time = time.time()
            if loop_length == -1:
                note = (samples[channel], time.time() - start_time)
                first_loop_notes.append(note)
            elif in_loop_mode:
                t = threading.Timer(loop_length, repeat_note, [samples[channel], len(loops)-1, len(loops[len(loops)-1])])
                t.start()
                loops[len(loops)-1].append(t)

def repeat_note(sample, loop_index, note_index):
    global loops
    t = threading.Timer(loop_length, repeat_note, [sample, loop_index, note_index])
    t.start()
    loops[loop_index][note_index] = t
    sample.play(loops=0)

def layer_loop():
    global loops
    layering_thread = threading.Timer(loop_length, layer_loop);
    layering_thread.start()
    if len(loops[len(loops)-1]) != 0:
        loops.append([])

def handle_instrument(channel, pressed):
    global loop_length, first_loop_notes, instrument_pressed, loops
    if pressed:
        if loop_length == -1:
            loop_length = time.time() - start_time
            for i, note in enumerate(first_loop_notes):
                t = threading.Timer(note[1], repeat_note, [note[0], 0, i])
                t.start()
                loops[0].append(t)
            loops.append([])
            layering_thread = threading.Timer(loop_length, layer_loop);
            layering_thread.start()
        elif not instrument_pressed:
            instrument_pressed = True

def handle_octave_up(channel, pressed):
    global octave
    if pressed and octave < octaves:
        octave += 1
        print('Selected Octave: {}'.format(octave))

def handle_octave_down(channel, pressed):
    global octave
    if pressed and octave > 0:
        octave -= 1
        print('Selected Octave: {}'.format(octave))

pianohat.on_note(handle_note)
pianohat.on_octave_up(handle_octave_up)
pianohat.on_octave_down(handle_octave_down)
pianohat.on_instrument(handle_instrument)

load_samples(patches[patch_index])
pianohat.auto_leds(True)

print("Ready to loop.")
signal.pause()
